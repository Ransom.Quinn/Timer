
import sys
import json 
import os

from PySide import QtGui, QtUiTools, QtCore, QtXml

class Main_Window(QtGui.QMainWindow):
    def __init__(self, *args):
        apply(QtGui.QMainWindow.__init__, (self,) + args)
        global settings
        loader = QtUiTools.QUiLoader()
        self.main_ui = loader.load('Example.ui', self)
        self.setCentralWidget(self.main_ui)
        self.setWindowTitle('Example')
        self.setUnifiedTitleAndToolBarOnMac(True)
        window = self

    def quit(self):
        print("Ended Session")

if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    win = Main_Window()
    win.show() #How do I use win??????
    app.connect(app, QtCore.SIGNAL('aboutToQuit()'),win.quit)
    app.exec_()